﻿
using QuanLySinhVien;

Console.OutputEncoding = System.Text.Encoding.Unicode;

QuanLy quanLy = new QuanLy();
while (true)
{
    Console.WriteLine("Menu:");
    Console.WriteLine("1. Thêm thông tin sinh viên");
    Console.WriteLine("2. Thêm thông tin môn học");
    Console.WriteLine("3. Nhập thông môn học và điểm của sinh viên");
    Console.WriteLine("4. Thống kê danh sách sinh viên và số lượng môn học");
    Console.WriteLine("5. Thống kê môn học có bao nhiêu sinh viên");
    Console.WriteLine("6. Thống kê số lượng sinh viên");
    Console.WriteLine("7. Thống kê số lượng môn học có sinh viên");
    Console.WriteLine("8. Thống kê số lượng môn học không có sinh viên");
    Console.WriteLine("9. Thống kê điểm trung bình sinh viên");
    Console.WriteLine("0. Thoát");

    Console.Write("Chọn chức năng: ");
    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            quanLy.TaoSinhVien();
            break;

        case "2":
            quanLy.TaoMonHoc();
            break;

        case "3":
            quanLy.DangKyMonHoc();
            break;

        case "4":
            quanLy.ThongKeDanhSachSinhVienVaMonHoc();
            break;

        case "5":
            quanLy.ThongKeDanhMonHocVaSoSinhVien();
            break;

        case "6":
            quanLy.ThongKeTongSoLuongSinhVien();
            break;

        case "7":
            quanLy.ThongKeMonHocCoSinhVien();
            break;

        case "8":
            quanLy.ThongKeMonHocKhongCoSinhVien();
            break;
        case "9":
            quanLy.TinhDiemTrungBinhSinhVien();
            break;
        case "0":
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Chức năng không hợp lệ.");
            break;
    }
}