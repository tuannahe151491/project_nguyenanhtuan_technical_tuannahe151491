﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{
    internal class QuanLy
    {
        //List<SinhVien> danhSachSinhVien = new List<SinhVien>();
        //List<MonHoc> danhSachMonHoc = new List<MonHoc>();
        //List<DangKy> danhSachDangKy = new List<DangKy>();

        List<SinhVien> danhSachSinhVien = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Nguyễn Anh Tuấn", Tuoi = 22, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Lương Thanh Thảo", Tuoi = 21, Khoa = "Marketing" },
            new SinhVien { MaSinhVien = 3, HoTen = "Điêu Quý Duy", Tuoi = 22, Khoa = "Ngôn ngữ Hàn" },
            new SinhVien { MaSinhVien = 4, HoTen = "Phạm Văn Dương", Tuoi = 23, Khoa = "Ngôn ngữ Nhật" },
            new SinhVien { MaSinhVien = 5, HoTen = "Nhật Linh", Tuoi = 21, Khoa = "Ngôn ngữ Anh" }
        };

        List<MonHoc> danhSachMonHoc = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "C" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "MAS291" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "WDU202" }
        };

        List<DangKy> danhSachDangKy = new List<DangKy>
        {
            new DangKy { MaSinhVien = 1, MaMonHoc = 101, Diem = 7 },
            new DangKy { MaSinhVien = 1, MaMonHoc = 102, Diem = 8 },
            new DangKy { MaSinhVien = 2, MaMonHoc = 101, Diem = 8 },
            new DangKy { MaSinhVien = 2, MaMonHoc = 102, Diem = 9 },
            new DangKy { MaSinhVien = 3, MaMonHoc = 101, Diem = 8 },
            new DangKy { MaSinhVien = 4, MaMonHoc = 103, Diem = 7 },
            new DangKy { MaSinhVien = 5, MaMonHoc = 102, Diem = 9 }
            //new DangKy { MaSinhVien = 5, MaMonHoc = 103, Diem = 5 }
        };


        public void TaoSinhVien()
        {
            SinhVien sinhVien = new SinhVien();
            int mSV = 0;
            var countSV = 0;
            do
            {
                Console.WriteLine("Mời bạn nhập mã sinh viên");
                mSV = Int32.Parse(Console.ReadLine());
                countSV = danhSachSinhVien.Where(x => x.MaSinhVien == mSV).Count();
            } while (countSV > 0);
            sinhVien.MaSinhVien = mSV;
            Console.WriteLine("Mời bạn nhập tên sinh viên");
            sinhVien.HoTen = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập tuổi sinh viên");
            sinhVien.Tuoi = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập khoa");
            sinhVien.Khoa = Console.ReadLine();
            danhSachSinhVien.Add(sinhVien);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                TaoSinhVien();
            }
        }

        public void TaoMonHoc()
        {
            MonHoc monHoc = new MonHoc();

            int mMH = 0;
            var countMH = 0;
            do
            {
                Console.WriteLine("Mời bạn nhập mã môn học");
                mMH = Int32.Parse(Console.ReadLine());
                countMH = danhSachMonHoc.Where(x => x.MaMonHoc == mMH).Count();
            } while (countMH > 0);
            monHoc.MaMonHoc = mMH;
            Console.WriteLine("Mời bạn nhập tên môn học");
            monHoc.TenMonHoc = Console.ReadLine();
            danhSachMonHoc.Add(monHoc);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                TaoMonHoc();
            }
        }

        public void DangKyMonHoc()
        {
            DangKy dangKy = new DangKy();
            int checkSv = 0;
            int maSV = 0;
            int checkMH = 0;
            int maMH = 0;

            do
            {
                Console.WriteLine("Nhập mã sinh viên: ");
                maSV = Int32.Parse(Console.ReadLine());
                checkSv = danhSachSinhVien.Where(x => x.MaSinhVien == maSV).Count();

            } while (checkSv == 0);
            do
            {
                Console.WriteLine("Nhập mã môn học: ");
                maMH = Int32.Parse(Console.ReadLine());
                checkMH = danhSachMonHoc.Where(x => x.MaMonHoc == maMH).Count();

            } while (checkSv == 0);

            int checkDuyNhat = danhSachDangKy
                                .Where(x => (x.MaMonHoc == maMH) && (x.MaSinhVien == maSV))
                                .Count();
            if (checkDuyNhat == 1)
            {
                Console.WriteLine("Sinh viên đã đăng ký môn học");
                return;
            }

            dangKy.MaSinhVien = maSV;
            dangKy.MaMonHoc = maMH;
            Console.WriteLine("Nhập điểm sinh viên: ");
            dangKy.Diem = float.Parse(Console.ReadLine());
            danhSachDangKy.Add(dangKy);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                DangKyMonHoc();
            }
        }

        public void ThongKeDanhSachSinhVienVaMonHoc()
        {
            var thongKeDanhSachSinhVienVaMonHoc = from dk in danhSachDangKy
                                                  join sv in danhSachSinhVien on dk.MaSinhVien equals sv.MaSinhVien
                                                  group dk by sv.HoTen into NhomTenSV
                                                  select new
                                                  {
                                                      SV = NhomTenSV.Key,
                                                      TongMonHoc = NhomTenSV.Count(),
                                                  };
            foreach (var sv in thongKeDanhSachSinhVienVaMonHoc)
            {
                Console.WriteLine("Sinh vien: " + sv.SV + " | So luong mon hoc: " + sv.TongMonHoc);
            }


            var rightJoinQuery = from leftItem in danhSachDangKy
                                 join rightItem in danhSachSinhVien
                                 on leftItem.MaSinhVien equals rightItem.MaSinhVien into rightGroup
                                 from result in rightGroup.DefaultIfEmpty()
                                 select new
                                 {
                                     LeftItem = leftItem,
                                     RightItem = result
                                 };
            foreach (var item in rightJoinQuery)
            {
                Console.WriteLine(item.LeftItem.MaSinhVien + "|" + item.RightItem.HoTen);
            }

            var query = from d in danhSachSinhVien
                        join e in danhSachDangKy
                        on d.MaSinhVien equals e.MaSinhVien into ed
                        //from e in ed.DefaultIfEmpty()
                        select new
                        {
                            MaMH = d.MaSinhVien,
                            SoLuong = ed.Count()
                        };

            foreach (var result in query)
            {
                Console.WriteLine($"Sinh vien {result.MaMH} va so luong mon hoc {result.SoLuong}");
            }
        }

        public void ThongKeDanhMonHocVaSoSinhVien()
        {
            var thongKeDanhMonHocVaSoSinhVien = from dk in danhSachDangKy
                                                group dk by dk.MaMonHoc into NhomMaMH
                                                select new
                                                {
                                                    MH = NhomMaMH.Key,
                                                    TongSinhVien = NhomMaMH.Count(),
                                                };
            foreach (var mh in thongKeDanhMonHocVaSoSinhVien)
            {
                Console.WriteLine("Mon hoc: " + mh.MH + " | So luong sinh vien: " + mh.TongSinhVien);
            }
        }

        public void ThongKeTongSoLuongSinhVien()
        {
            int countSV = danhSachSinhVien.Count();
            Console.WriteLine("Tổng số sinh viên: " + countSV);
        }

        public void ThongKeMonHocCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from dk in danhSachDangKy
                                          group dk by dk.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };

            Console.WriteLine("So mon hoc co sinh vien: " + thongKeMonHocCoSinhVien.Count());
        }

        public void ThongKeMonHocKhongCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from dk in danhSachDangKy
                                          group dk by dk.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };
            Console.WriteLine("So mon hoc khong co sinh vien: " + (danhSachMonHoc.Count() - thongKeMonHocCoSinhVien.Count()));
        }

        public void TinhDiemTrungBinhSinhVien()
        {
            foreach (SinhVien sv in danhSachSinhVien)
            {
                float tongDiem = 0;
                int soMon = 0;

                foreach (DangKy dk in danhSachDangKy)
                {
                    if (dk.MaSinhVien == sv.MaSinhVien)
                    {
                        tongDiem += dk.Diem;
                        soMon++;
                    }
                }

                float diemTrungBinh = tongDiem / soMon;
                Console.WriteLine("Sinh vien " + sv.MaSinhVien + " co diem trung binh: " + diemTrungBinh);
            }
        }
    }
}
