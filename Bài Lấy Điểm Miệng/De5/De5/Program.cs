﻿using De5;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validate validate = new Validate();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("-------------------------------------------");
    Console.WriteLine("1.Nhập 1 danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất các đối tượng có mã bắt đầu bằng chữ “CO”");
    Console.WriteLine("4.Sắp xếp năm phát hiện chủng covid mới theo thứ tự tăng dần");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 4);

    switch (luaChon)
    {
        case 1:
            service.AddCovid();
            break;
        case 2:
            service.ShowCovid();
            break;
        case 3:
            service.ShowCovidStartCO();
            break;
        case 4:
            service.SortByYear();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 4);
