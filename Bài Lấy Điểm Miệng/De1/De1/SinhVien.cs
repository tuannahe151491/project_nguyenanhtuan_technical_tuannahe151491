﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De1
{
    internal class SinhVien
    {
        public string MaSV {  get; set; }
        public string Ten {  get; set; }
        public int NamSinh {  get; set; }

        public virtual string InThongTin()
        {
            return string.Format("{0, -15} {1, -20} {2, -15}", MaSV, Ten, NamSinh);
        }

        public SinhVien() { }
        public SinhVien(string maSV, string ten, int namSinh)
        {
            MaSV = maSV;
            Ten = ten;
            NamSinh = namSinh;
        }
        
    }
}
