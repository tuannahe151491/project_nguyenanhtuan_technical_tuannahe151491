using De1;
using System.Text;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validate validate = new Validate();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("-------------------------------------------");
    Console.WriteLine("Chương trình quản lý sinh viên");
    Console.WriteLine("1.Nhập 1 danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất danh sách các SV tuổi từ 50 trở lên");
    Console.WriteLine("4.Tìm SV theo mã");
    Console.WriteLine("5.Kế thừa");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 5);

    switch (luaChon)
    {
        case 1:
            service.AddSinhVien();
            break;
        case 2:
            service.ShowSinhVien();
            break;
        case 3:
            service.ListSinhVienHigher50Age();
            break;
        case 4:
            service.SearchSinhVien();
            break;
        case 5:
            service.InheritSinhVien();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 5);
