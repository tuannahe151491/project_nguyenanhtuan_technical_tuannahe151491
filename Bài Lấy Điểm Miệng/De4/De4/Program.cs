﻿using De4;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Validation validation = new Validation();
int choice;

Service service = new Service();
service.Init();
do
{
    Console.WriteLine("=========MENU =========");
    Console.WriteLine("1. Nhập danh sách đối tượng");
    Console.WriteLine("2. Xuất danh sách đối tượng");
    Console.WriteLine("3. Xoá đối tượng theo mã laptop");
    Console.WriteLine("4. Xuất máy tính theo khoảng màn hình");
    Console.WriteLine("0. Thoát");
    choice = validation.InputIntegerInRange("Chọn chức năng: ", 0, 5);
    switch (choice)
    {
        case 1:
            service.NhapDanhSachDoiTuong();
            break;
        case 2:
            service.XuatDoiTuong();
            break;
        case 3:
            service.XoaLapTopTheoMa();
            break;
        case 4:
            service.XuatLapTopTheoKhoangManHinh();
            break;
        default:
            break;
    }
} while (choice > 0 && choice <= 4);
Console.ReadKey();
