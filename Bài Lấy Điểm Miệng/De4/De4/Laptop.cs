﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De4
{
    internal class LapTop
    {
        public int Id { get; set; }
        public string MaLapTop { get; set; }
        public double KichThuocMH { get; set; }

        public LapTop()
        {
        }

        public LapTop(int id, string maLapTop, double kichThuocMH)
        {
            Id = id;
            MaLapTop = maLapTop;
            KichThuocMH = kichThuocMH;
        }

        public void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", this.Id, this.MaLapTop, this.KichThuocMH));
        }
    }

}
