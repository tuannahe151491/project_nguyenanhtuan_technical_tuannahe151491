﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Student
    {
        public int Id { get; set; }
        public string Ten {  get; set; }
        public int Tuoi { get; set; }
        public string Nganh { get; set; }

        public Student() { }

        public Student(int id, string ten, int tuoi, string nganh)
        {
            Id = id;
            Ten = ten;
            Tuoi = tuoi;
            Nganh = nganh;
        }

        public virtual void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -15} {1, -20} {2, -10} {3, -10}", Id, Ten, Tuoi, Nganh));
        }
    }
}
