﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De10
{
    internal class NganhHoc
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public int SoKyHoc { get; set; }

        public NganhHoc()
        {
        }

        public NganhHoc(int id, string ten, int soKyHoc)
        {
            Id = id;
            Ten = ten;
            SoKyHoc = soKyHoc;
        }

        public void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -20}{2, -10}", this.Id, this.Ten, this.SoKyHoc));
        }
    }

}
