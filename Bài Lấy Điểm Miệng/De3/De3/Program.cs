﻿using De3;
using System.Text;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validate validate = new Validate();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("-------------------------------------------");
    Console.WriteLine("1.Nhập 1 danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3. Xóa đối tượng theo ID");
    Console.WriteLine("4. Xuất trọng lượng máy tính theo tên gần đúng");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 4);

    switch (luaChon)
    {
        case 1:
            service.AddMayTinh();
            break;
        case 2:
            service.ShowMayTinh();
            break;
        case 3:
            service.DeleteById();
            break;
        case 4:
            service.FindByName();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 4);
