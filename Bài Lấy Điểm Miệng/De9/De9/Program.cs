﻿using De9;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Validate validate = new Validate();
Service service = new Service();
service.Init();
do
{
    Console.WriteLine("-------------------------------------------");
    Console.WriteLine("1.Nhập 1 danh sách đối tượng");
    Console.WriteLine("2.Xuất danh sách đối tượng");
    Console.WriteLine("3.Xuất các xe máy của hãng HONDA");
    Console.WriteLine("4.Sắp xếp đối tượng theo ID giảm dần");
    Console.WriteLine("0. Thoát.");
    luaChon = validate.InputInt("Mời bạn nhập lựa chọn: ", 0, 4);

    switch (luaChon)
    {
        case 1:
            service.AddBike();
            break;
        case 2:
            service.ShowBike();
            break;
        case 3:
            service.ShowHONDA();
            break;
        case 4:
            service.SortById();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 4);
