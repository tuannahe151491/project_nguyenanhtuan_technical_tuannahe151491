﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De6
{
    internal class HocSinhNamSinh : HocSinh
    {
        int NamSinh { get; set; }

        public HocSinhNamSinh()
        {
        }

        public HocSinhNamSinh(string maHs, string ten, int tuoi) : base(maHs, ten, tuoi)
        {
            this.NamSinh = DateTime.Now.Year - tuoi;
        }

        public override void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -10}{1, -30}{2, -5}{3, -10}", this.MaHs, this.Ten, this.Tuoi, this.NamSinh));
        }

    }

}
