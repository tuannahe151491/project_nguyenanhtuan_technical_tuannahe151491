﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De8
{
    internal class Teacher
    {
        public int Id { get; set; }
        public string MaGV { get; set; }
        public string Nganh { get; set; }

        public Teacher()
        {
        }

        public Teacher(int id, string maGV, string nganh)
        {
            Id = id;
            MaGV = maGV;
            Nganh = nganh;
        }

        public void InThongTin()
        {
            Console.WriteLine(string.Format("{0, -5}{1, -10}{2, -20}", this.Id, this.MaGV, this.Nganh));
        }

    }
}
