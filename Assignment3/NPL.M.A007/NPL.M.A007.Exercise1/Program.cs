﻿// See https://aka.ms/new-console-template for more information
using System.Text;
using NPL.M.A007.Exercise1;
Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}", "Book Name", "ISBN", "Author Nam", "Publisher Name"));
Book book = new Book("Harry Potter", 123456, "J.K Rowling", "Kim Dong");
Book book2 = new Book("Giông Tố", 123457, "Vũ Trọng Phụng", "Kim Đồng");
Console.WriteLine(book.GetBookInformation());
Console.WriteLine(book2.GetBookInformation());
Console.ReadKey();

