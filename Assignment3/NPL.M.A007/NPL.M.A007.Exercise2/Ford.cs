﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Ford : Car
    {
        public int Year { get; set; }
        public int ManufacturerDiscount { get; set; }

        public Ford() { }

        public Ford(decimal speed, double regularPrice, string color, int year, int ManufacturerDiscount) : base(speed, regularPrice, color)
        {
            this.Year = year;
            this.ManufacturerDiscount = ManufacturerDiscount;
        }

        public override double GetSalePrice()
        {
            return this.RegularPrice - ManufacturerDiscount;
        }
        public void DisplayPrice()
        {
            Console.WriteLine(Color + " " + "Ford Price: " + this.GetSalePrice());
        }

    }
}
