﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Validate
    {
        public int checkIdEmployessExist(List<Employee> list, string mess)
        {
            Console.Write(mess);
            int id = 0;
            do
            {
                id = int.Parse(Console.ReadLine());

                bool isIdDuplicate = false;
                foreach (Employee emp in list)
                {
                    if (id.Equals(emp.EmployeeID))
                    {
                        isIdDuplicate = true;
                        break;
                    }
                }
                if (isIdDuplicate)
                {
                    Console.WriteLine("ID is already exist!");
                    Console.Write(mess);
                }
                else
                {
                    break;
                }

            } while (true);
            return id;
        }
        public int checkIdLanguaeExist(List<ProgrammingLanguage> list, string mess)
        {
            Console.Write(mess);
            int id = 0;
            do
            {
                id = int.Parse(Console.ReadLine());

                bool isIdDuplicate = false;
                foreach (ProgrammingLanguage pl in list)
                {
                    if (id.Equals(pl.LanguageID))
                    {
                        isIdDuplicate = true;
                        break;
                    }
                }
                if (isIdDuplicate)
                {
                    Console.WriteLine("ID is already exist!");
                    Console.Write(mess);
                }
                else
                {
                    break;
                }

            } while (true);
            return id;
        }
        public int checkIdDepartmentExist(List<Department> list, string mess)
        {
            Console.Write(mess);
            int id = 0;
            do
            {
                id = int.Parse(Console.ReadLine());

                bool isIdDuplicate = false;
                foreach (Department department in list)
                {
                    if (id.Equals(department.DepartmentID))
                    {
                        isIdDuplicate = true;
                        break;
                    }
                }
                if (isIdDuplicate)
                {
                    Console.WriteLine("ID is already exist!");
                    Console.Write(mess);
                }
                else
                {
                    break;
                }

            } while (true);
            return id;
        }
        public int checkIntLimit(string mess, int a, int b)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());
                    if (input >= a && input <= b)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Must be in " + a + "-" + b);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Option invalid!");
                }
            } while (true);
            return input;
        }
        public bool CheckYesNo(string message)
        {
            while (true)
            {
                Console.Write(message);
                string input = Console.ReadLine();
                if (input.ToLower().Equals("y"))
                {
                    return true;
                }
                else if (input.ToLower().Equals("n"))
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Enter Y/y Or N/n!: ");
                }
            }
        }
        public string CheckString(string message, string pattern)
        {
            string input = string.Empty;
            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Invalid");
                    continue;
                }
                else
                {
                    break;
                }
            }

            return input;
        }
        
        public int CheckInt(string mess)
        {
            int input = 0;
            do
            {
                try
                {
                    Console.Write(mess);
                    input = int.Parse(Console.ReadLine());

                    if (input < 0)
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid! ");
                }
            } while (true);

            return input;
        }
        public string CheckString(string mess)
        {
            string input = "";
            do
            {
                try
                {
                    Console.Write(mess);
                   input = Console.ReadLine();

                    if (string.IsNullOrEmpty(input))
                    {
                        throw new Exception();
                    }

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid! ");
                }
            } while (true);

            return input;
        }
        public DateTime CheckDate(String message)
        {
            string input = string.Empty;
            DateTime date;

            while (true)
            {
                Console.Write(message);
                input = Console.ReadLine();
                try
                {
                    date = DateTime.ParseExact(input, "dd/MM/yyyy", null);
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid input!");
                    continue;
                }
            }
            return date;
        }

    }
}
