﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NPL.M.A015
{
    internal class Department
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public Department()
        {
        }

        public Department(int departmentID, string name)
        {
            DepartmentID = departmentID;
            DepartmentName = DepartmentName;

        }
    }
}
