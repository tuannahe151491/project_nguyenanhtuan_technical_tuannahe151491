﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Manager
    {
        Validate validate = new Validate();
        List<Employee> employees = new List<Employee>();
        List<ProgrammingLanguage> languages = new List<ProgrammingLanguage>();
        List<Department> departments = new List<Department>();
        List<Skill> skills = new List<Skill>();
        public void InputData()
        {
            employees.Add(new Employee { EmployeeID = 1, EmployeeName = "Nguyen Anh Tuan", Age = 22, Status = true, DepartmentID = 101 });
            employees.Add(new Employee { EmployeeID = 2, EmployeeName = "Nguyễn Anh Tai", Age = 21, Status = true, DepartmentID = 102 });
            employees.Add(new Employee { EmployeeID = 3, EmployeeName = "Nguyễn Duc Canh", Age = 20, Status = false, DepartmentID = 103 });
            employees.Add(new Employee { EmployeeID = 4, EmployeeName = "Luong Thanh Thao", Age = 21, Status = true, DepartmentID = 101 });
            employees.Add(new Employee { EmployeeID = 5, EmployeeName = "Nguyen Trong Huynh", Age = 23, Status = true, DepartmentID = 102 });

            languages.Add(new ProgrammingLanguage { LanguageID = 1, LanguageName = "Java" });
            languages.Add(new ProgrammingLanguage { LanguageID = 2, LanguageName = "C#" } );
            languages.Add(new ProgrammingLanguage { LanguageID = 3, LanguageName = "HTML" });
            languages.Add(new ProgrammingLanguage { LanguageID = 4, LanguageName = "C" });

            departments.Add(new Department { DepartmentID = 101, DepartmentName = "DEV" });
            departments.Add(new Department { DepartmentID = 102, DepartmentName = "INTERN" });
            departments.Add(new Department { DepartmentID = 103, DepartmentName = "TESTER" });
            departments.Add(new Department { DepartmentID = 104, DepartmentName = "FA" });

            skills.Add(new Skill { EmployeeID = 1, LanguageID = 1 });
            skills.Add(new Skill { EmployeeID = 2, LanguageID = 1 });
            skills.Add(new Skill { EmployeeID = 2, LanguageID = 2 });
            skills.Add(new Skill { EmployeeID = 3, LanguageID = 2 });
            skills.Add(new Skill { EmployeeID = 4, LanguageID = 4 });
            skills.Add(new Skill { EmployeeID = 5, LanguageID = 3 });
        }
        public void InputEmployee()
        {
            do
            {
                int employeeID = validate.checkIdEmployessExist(employees, "Enter ID: ");
                string name = validate.CheckString("Enter employee name: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                int age  = validate.CheckInt("Enter Employee age: ");
                string address = validate.CheckString("Enter adrress: ");
                DateTime hiredDate = validate.CheckDate("Enter Hired Date: ");
                int departmentID = validate.CheckInt("Enter departmentID: ");
                bool status= validate.CheckYesNo("Enter status (Y/y or N/n): ");
                

                Employee emp = new Employee(employeeID,name, age, address, hiredDate, status,departmentID);
                employees.Add(emp);
            } while (validate.CheckYesNo("Do you want add more?(Y/N): "));
            
        }
        public void InputLanguage()
        {
            do
            {
                int ID = validate.checkIdLanguaeExist(languages, "Enter ID: ");
                string ten = validate.CheckString("Enter name language: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                ProgrammingLanguage language = new ProgrammingLanguage(ID,ten);
                languages.Add(language);
            } while (validate.CheckYesNo("Do you want add more?(Y/N): "));
        }
        public void InputDepartment()
        {
            do
            {
                int ID = validate.checkIdDepartmentExist(departments, "Enter ID: ");
                string ten = validate.CheckString("Enter name: ", "^[a-zA-ZÀ-ỹ\\s]+$");
                Department department = new Department(ID, ten);
                departments.Add(department);
            } while (validate.CheckYesNo("Do you want add more?(Y/N): "));
        }
        public void InputSkill()
        {
            do
            {
                int employeeID = validate.CheckInt( "Enter employee ID: ");
                int languageID = validate.CheckInt("Enter language ID: ");
                Skill skill = new Skill(employeeID, languageID);
                skills.Add(skill);
            } while (validate.CheckYesNo("Do you want add more?(Y/N): "));
        }
        public List<Department> GetDepartments(List<Department> departments, List<Employee> employees, int numberOfEmployees)
        {
            var result = new List<Department>();
            foreach (var department in departments)
            {
                int count = employees.Count(e => e.DepartmentID == department.DepartmentID);
                if (count >= numberOfEmployees)
                {
                    result.Add(department);
                }
            }
            return result;
        }

        
        public void case5()
        {
            int input = validate.CheckInt("Enter number of Employee: ");
            List<Department> result1 = GetDepartments(departments, employees, input);
            Console.WriteLine("Departments with at least "+input+" employee:");
            foreach (var department in result1)
            {
                Console.WriteLine(department.DepartmentName);
            }
        }

        
        public List<Employee> GetEmployeesWorking()
        {
            List<Employee> result = new List<Employee>();
            foreach (var employee in employees)
            {
                if (employee.Status)
                {
                    result.Add(employee);
                }
            }
            return result;
        }
        public void case6()
        {
            var workingEmployees = GetEmployeesWorking();
            foreach (var employee in workingEmployees)
            {
                Console.WriteLine("ID: "+ employee.EmployeeID +"|  Name: "+employee.EmployeeName );
            }
        }
        
        public List<Employee> GetEmployees(string languageName)
        {
            List<Employee> result = new List<Employee>();
            int languageId = -1;
            foreach (var language in languages)
            {
                if (language.LanguageName.ToLower() == languageName.ToLower())
                {
                    languageId = language.LanguageID;
                    break;
                }
            }
            if (languageId != -1)
            {
                foreach (var emp in employees)
                {
                    bool knowsLanguage = false;
                    foreach (var skill in skills)
                    {
                        if (skill.EmployeeID == emp.EmployeeID && skill.LanguageID == languageId)
                        {
                            knowsLanguage = true;
                            break;
                        }
                    }
                    if (knowsLanguage)
                    {
                        result.Add(emp);
                    }
                }
            }
            return result;
        }
        public void case7()
        {
            string input = validate.CheckString("Enter language name: ");
            
            var employeesKnowLanguage = GetEmployees(input);
            if(employeesKnowLanguage.Count==0)
            {
                Console.WriteLine("Not found this language name");
            }
            else {
                Console.WriteLine($"Employees who know {input}:");
                foreach (var employee in employeesKnowLanguage)
                {
                    Console.WriteLine("ID: " + employee.EmployeeID + "|  Name: " + employee.EmployeeName);
                }
            }
            
        }
        
        public List<ProgrammingLanguage> GetLanguages(int employeeId)
        {
            List<ProgrammingLanguage> result = new List<ProgrammingLanguage>();
            foreach (var language in languages)
            {
                bool knowsLanguage = false;
                foreach (var skill in skills)
                {
                    if (skill.EmployeeID == employeeId && skill.LanguageID == language.LanguageID)
                    {
                        knowsLanguage = true;
                        break;
                    }
                }
                if (knowsLanguage)
                {
                    result.Add(language);
                }
            }
            return result;
        }
        public void case8()
        {
            int employeeId = validate.CheckInt("Enter employee ID: ");
            bool check = false;
            foreach (var item in employees)
            {
                if (item.EmployeeID.Equals(employeeId))
                {
                    check=true;
                    var knownLanguages = GetLanguages(employeeId);
                    if (knownLanguages.Count == 0)
                    {
                        Console.WriteLine("Not found");
                    }
                    else
                    {
                        
                        foreach (var language in knownLanguages)
                        {
                            Console.WriteLine(language.LanguageName + " | ID: " + language.LanguageID);
                        }
                    }
                }
            }
            if (!check)
            {
                Console.WriteLine("Not found this employee");
            }
            
            
        }
        public List<Employee> GetEmployeesKnowMultipleLanguage()
        {
            List<Employee> result = new List<Employee>();
            foreach (var employee in employees)
            {
                int languageCount = 0;
                foreach (var skill in skills)
                {
                    if (skill.EmployeeID == employee.EmployeeID)
                    {
                        languageCount++;
                        if (languageCount >= 2)
                        {
                            result.Add(employee);
                            break;
                        }
                    }
                }
            }
            return result;
        }
        public void case9()
        {
            var seniorEmployees = GetEmployeesKnowMultipleLanguage();
            Console.WriteLine("Employees who know multiple programming languages:");
            foreach (var employee in seniorEmployees)
            {
                Console.WriteLine("ID: " + employee.EmployeeID + "|  Name: " + employee.EmployeeName);
            }
        }
        public List<Employee> GetEmployeePaging(int pageIndex, int pageSize, string employeeName, string order)
        {
            List<Employee> result = new List<Employee>();
            List<Employee> filteredEmployees = employees;
            if (!string.IsNullOrEmpty(employeeName))
            {
                filteredEmployees = filteredEmployees.Where(e => e.EmployeeName.ToLower().Contains(employeeName.ToLower())).ToList();
            }

            if (order == "ASC")
            {
                filteredEmployees = filteredEmployees.OrderBy(e => e.EmployeeName).ToList();
            }
            else if (order == "DESC")
            {
                filteredEmployees = filteredEmployees.OrderByDescending(e => e.EmployeeName).ToList();
            }

            int start = (pageIndex - 1) * pageSize;
            int end = Math.Min(start + pageSize, filteredEmployees.Count);

            for (int i = start; i < end; i++)
            {
                result.Add(filteredEmployees[i]);
            }

            return result;
        }
        public void case10()
        {
            int pageIndex = validate.CheckInt("Enter pageIndext: ");
            int pageSize = validate.CheckInt("Enter pageSize: ");
            string employeeName = validate.CheckString("Enter name employee: ", "^[a-zA-ZÀ-ỹ\\s]+$");
            string order = validate.CheckString("Enter ASD OR DESC: ");
            var employeesPaging = GetEmployeePaging(pageIndex, pageSize, employeeName, order);
            if (employeesPaging.Count == 0)
            {
                Console.WriteLine("Not found any employee");
            }
            else
            {
                Console.WriteLine($"\nEmployee Paging (Page {pageIndex}, Page Size {pageSize}, Order {order}):");
                foreach (var employee in employeesPaging)
                {
                    Console.WriteLine("ID: " + employee.EmployeeID + "|  Name: " + employee.EmployeeName);
                }
            }
            
        }
        public List<KeyValuePair<Department, List<Employee>>> GetDepartmentsWithEmployees()
        {
            var result = new List<KeyValuePair<Department, List<Employee>>>();
            foreach (var department in departments)
            {
                var departmentEmployees = employees.Where(e => e.DepartmentID == department.DepartmentID).ToList();
                result.Add(new KeyValuePair<Department, List<Employee>>(department, departmentEmployees));
            }
            return result;
        }
        public void case11()
        {
            List<KeyValuePair<Department, List<Employee>>> departmentsWithEmployees = GetDepartmentsWithEmployees();

            
            foreach (var departmentWithEmployees in departmentsWithEmployees)
            {
                Console.WriteLine("Department: "+ departmentWithEmployees.Key.DepartmentName);
                foreach (var employee in departmentWithEmployees.Value)
                {
                    Console.WriteLine(" - Employee Name: "+ employee.EmployeeName);
                }
                Console.WriteLine();
            }
        }
    }
}
