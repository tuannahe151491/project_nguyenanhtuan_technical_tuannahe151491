﻿// See https://aka.ms/new-console-template for more information

using NPL.M.A015.Exercise;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;
int choice;
Validate validate = new Validate();
Manager manager = new Manager();
manager.InputData();
do
{
    Console.WriteLine("Lựa Chọn");
    Console.WriteLine("1.Input employee.");
    Console.WriteLine("2.Input programing language.");
    Console.WriteLine("3.Input department");
    Console.WriteLine("4.Input skill of employee");
    Console.WriteLine("5.Get list department have number of employee user enter: ");
    Console.WriteLine("6.List all employees are working.");
    Console.WriteLine("7.List all employees know languageName");
    Console.WriteLine("8.Programing languages which employee has employeeld know.");
    Console.WriteLine("9.Employees who know multiple programming languages.");
    Console.WriteLine("10.List of employees where pagelndex is the current page.");
    Console.WriteLine("11.All departments including employees that belong to each department.");

    Console.WriteLine("0.Exit");
    choice = validate.checkIntLimit("Nhập lựa chọn: ", 0, 11);
    switch (choice)
    {
        case 1:
            manager.InputEmployee();
            break;
        case 2:
            manager.InputLanguage();
            break;
        case 3:
            manager.InputDepartment();
            break;
        case 4:
            manager.InputSkill();
            break;
        case 5:
            manager.case5();
            break;
        case 6:
            manager.case6();
            break;
        case 7:
            manager.case7();
            break;
        case 8:
            manager.case8();
            break;
        case 9:
            manager.case9();
            break;
        case 10:
            manager.case10();
            break;
        case 11:
            manager.case11();
            break;
        default:
            break;
    }
} while (choice != 0);
Console.ReadKey();



